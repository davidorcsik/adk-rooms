const Room = class {
	constructor(number, floor, offset, size) {
		this.number = number;
		this.floor = floor;
		this.offset = offset;
		this.size = size;
		this.residents = [];
		this.oldResidents = [];
		this.normalViewElement = null;
		this.simpleViewElement = null;
	}
	roomId() {
		return this.floor + ((this.number < 10)?"0":"") + this.number;
	}
	refresh() {
		let normalViewNewResidentContainers = $(this.normalViewElement).find("li");
		let simpleViewNewResidentContainers = $(this.simpleViewElement).find(".new_residence div");
		//let normalViewOldResidentContainers = $(this.normalViewElement).find("li");
		let simpleViewOldResidentContainers = $(this.simpleViewElement).find(".old_residence div");
		for (let i = 0; i<this.residents.length; ++i) {
			normalViewNewResidentContainers[i].innerHTML = this.residents[i];
			simpleViewNewResidentContainers[i].innerHTML = this.residents[i];
		}
		for (let i = 0; i<this.residents.length; ++i) {
			//normalViewOldResidentContainers[i].innerHTML = this.residents[i];
			simpleViewOldResidentContainers[i].innerHTML = this.oldResidents[i];
		}
	}
	reset() {
		$(this.normalViewElement).find("li").html("");
		$(this.simpleViewElement).find(".new_residence div").html("");
		this.residents = [];
	}
};

let building = [];

function calculateRoomX(baseOffset) {
	return (removeUnit($("#building").css("width")) * baseOffset) / 880;
}
function calculateRoomY(baseOffset) {
	return (removeUnit($("#building").css("height")) * baseOffset) / 1128;
}

function addRooms(building) {
	let zIndex = 99;
	for (let floor of building) {
		let floorTable = document.createElement("table");
		let floorTableCaption = document.createElement("caption");
		floorTableCaption.innerHTML = floor[0].floor + ". emelet";
		floorTable.appendChild(floorTableCaption);
		
		let floorContainer = document.createElement("article");
		$(floorContainer).css("z-index", zIndex);
		floorContainer.setAttribute("class", "floor");
		
		let floorColumnHeader;
		
		floorColumnHeader = document.createElement("th");
		floorColumnHeader.innerHTML = "Szoba";
		floorTable.appendChild(floorColumnHeader);
		
		floorColumnHeader = document.createElement("th");
		floorColumnHeader.innerHTML = "Régi lakók";
		floorTable.appendChild(floorColumnHeader);
		
		floorColumnHeader = document.createElement("th");
		floorColumnHeader.innerHTML = "Új lakók";
		floorTable.appendChild(floorColumnHeader);
		
		floorColumnHeader = document.createElement("th");
		floorTable.appendChild(floorColumnHeader);
		
		$(floorContainer).css("backgroundImage","url('res/pics/floor_" + floor[0].floor + ".svg')");
		for (let room of floor) {
			if (room.size.width === 0 && room.size.height === 0) continue;
			let roomRow = document.createElement("tr");			
			let roomCell;
			
			roomCell = document.createElement("td");
			roomCell.setAttribute("class", "number");
			roomCell.innerHTML = ((room.number < 10)? "0" : "") + room.number;
			roomRow.appendChild(roomCell);
			
			roomCell = document.createElement("td");
			roomCell.setAttribute("class", "old_residence");
			
			let oldResidenceElements = [];
			oldResidenceElements.push(document.createElement("div"));
			oldResidenceElements.push(document.createElement("div"));
			roomCell.appendChild(oldResidenceElements[0]);
			roomCell.appendChild(oldResidenceElements[1]);
			
			roomRow.appendChild(roomCell);
			
			roomCell = document.createElement("td");
			roomCell.setAttribute("class", "new_residence");
			
			let newResidenceElements = [];
			newResidenceElements.push(document.createElement("div"));
			newResidenceElements.push(document.createElement("div"));
			roomCell.appendChild(newResidenceElements[0]);
			roomCell.appendChild(newResidenceElements[1]);
			
			roomRow.appendChild(roomCell);
			
			
			roomCell = document.createElement("td");
			
			let roomActionButton = document.createElement("input");
			roomActionButton.setAttribute("type", "button");
			roomActionButton.value = "Jelentkezés";
			roomActionButton.dataset.level = room.floor;
			roomActionButton.dataset.number = room.number;
			
			roomCell.appendChild(roomActionButton);
			roomRow.appendChild(roomCell);
			
			floorTable.appendChild(roomRow);
			
			let roomElement = document.createElement("div");
			roomElement.dataset.level = room.floor;
			roomElement.dataset.number = room.number;
			room.normalViewElement = roomElement;
			room.simpleViewElement = roomRow;
			floorContainer.appendChild(roomElement);
			
			$(roomElement).css("top", convertPxToRem(calculateRoomY(room.offset.y)));
			$(roomElement).css("left", convertPxToRem(calculateRoomX(room.offset.x)));
			$(roomElement).css("width", convertPxToRem(calculateRoomX(room.size.width)));
			$(roomElement).css("height", convertPxToRem(calculateRoomY(room.size.height)));
			
			
			let roomNumberElement = document.createElement("span");
			roomNumberElement.innerHTML = room.roomId();
			roomElement.appendChild(roomNumberElement);
			
			let residentContainer = document.createElement("ul");
			residentContainer.appendChild(document.createElement("li"))
			residentContainer.appendChild(document.createElement("li"))
			
			roomElement.appendChild(residentContainer);
		}
		$("#building")[0].appendChild(floorContainer);
		$("#view_simple")[0].appendChild(floorTable);
		
		let floorSelectorElement = document.createElement("div");
		floorSelectorElement.innerHTML = floor[0].floor + ". em";
		$(floorSelectorElement).css("height", convertPxToRem(removeUnit($("#building_level_selector").css("height"))/building.length));
		
		$("#building_level_selector .levels")[0].appendChild(floorSelectorElement);
		$("#building_level_selector .levels").css("line-height", convertPxToRem(removeUnit($("#building_level_selector").css("height"))/building.length));

		--zIndex;
	}
}

function parseBuildingJSON(data) {
	for (let floor of data) {
		let temp = [];
		for (let room of floor.rooms) {
			temp.push(new Room(room.number, floor.level, room.offset, room.size));
		}
		building.push(temp);
	}
}

function loadAndParseBuildingJSON() {
	$.get({
		url: "res/building.json",
		contentType: "application/json",
		success: function(data) {
			parseBuildingJSON(data);
		},
		error: function(xhr, status) {
			console.log(status);
			console.error("Could not load layout json!");
		}
	}).done(function() {
		addRooms(building);
		appendFloorEventListeners();
	});
}

function parseOccupancy(data) {
	for (let floor of building) {
		for (let room of floor) room.reset();
	}
	for (let item of data) {
		building[building.length - item.level - 1][item.room_number - 1].residents.push(item.name);
	}
	for (let floor of building) {
		for (let room of floor) room.refresh();
	}
}

function parseOldOccupancy(data) {
	for (let item of data) {
		building[building.length - item.level - 1][item.room_number - 1].oldResidents.push(item.name);
	}
	for (let floor of building) {
		for (let room of floor) room.refresh();
	}
}