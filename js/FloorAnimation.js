let floorAnimationHoverDisabled = false;

function showFloor(floorIndex) {
	if ($("#page_overlay").hasClass("active")) return;
	let floors = $(".floor");
	if (floorIndex < 0 || floors.length <= floorIndex) return;
	floors.removeClass("active");
	floors.removeClass("offset-top");
	floors.removeClass("offset-bottom");
	
	if (floorIndex != 0) $(".floor:nth-child(" + floorIndex + ")").addClass("offset-bottom");
	$(".floor:nth-child(" + (floorIndex + 1) + ")").addClass("active");
	if (floorIndex != floors.length - 1) $(".floor:nth-child(" + (floorIndex + 2) + ")").addClass("offset-top");
	
	let floorContainerTopOffset;
	if (floorIndex == 0) floorContainerTopOffset = 30;
	else floorContainerTopOffset = -30 / floors.length * floorIndex;
	
	$("#building").css("top", floorContainerTopOffset + "%");
	$("#building_level_selector .overlay").css("top", convertPxToRem(removeUnit($("#building_level_selector").css("height")) / $(".floor").length * floorIndex));
}

function showPageOverlay(roomElement) {
	let room = building[building.length - roomElement.dataset.level - 1][roomElement.dataset.number - 1];
	if (room.residents.length === 2)  return;
	$("#page_overlay").addClass("active");
	$("#apply_box").fadeIn("fast");
	$("#apply_box .base_message .room_number_formated").html(roomNumberToFuckingHungarianRagozottForma(room.roomId()));

	$("#apply_box .apply")[0].dataset.room = room.roomId();

	let currentRoomNumberStr = $("#user_info #current_room").html();
	if (Number(currentRoomNumberStr) >= 0) {
		let floorNumber = Number(currentRoomNumberStr[0]);
		let currentNumber = Number(currentRoomNumberStr.substr(1, 2));
		let currentRoom = building[building.length - floorNumber - 1][currentNumber - 1];

		$("#apply_box .changing_room .room_number_formated").html(roomNumberToFuckingHungarianRagozottForma(currentRoom.roomId()));

		if (currentRoom.residents.length > 1) {
			let roommateName = currentRoom.residents[0];
			if ($("#user_info #name").html() === roommateName) roommateName = currentRoom.residents[1];
			$("#apply_box .changing_room .roommate_name").html(roommateName);
			$("#apply_box .changing_room .non_empty_suffix").css("display", "inline");
		} else {
			$("#apply_box .changing_room .non_empty_suffix").css("display", "none");
		}

		$("#apply_box .changing_room").css("display", "block");
	} else {
		$("#apply_box .changing_room").css("display", "none");
	}

	if (room.residents.length !== 0) {
		$("#apply_box .base_message .non_empty_suffix .roommate_name").html(room.residents[0]);
		$("#apply_box .base_message .non_empty_suffix").css("display", "inline")
	} else {
		$("#apply_box .base_message .non_empty_suffix").css("display", "none")
	}

}

function appendFloorEventListeners() {
	$(".floor:nth-child(1)").addClass("active");
	$(".floor:nth-child(2)").addClass("offset-top");
	
	$("#building_level_selector .levels div").click(function() {
		showFloor($(this).index());
	});

	$(".floor div").click(function() {
		showPageOverlay(this);
	});
	$("#view_simple table input").click(function() {
		showPageOverlay(this);
	});

	$("#user_info #room_checkout").click(function(e) {
		e.preventDefault();
		if ($("#user_info #current_room").html() === "-") return;
		dismiss_room();
	});

	document.addEventListener("wheel", function(event) {
		showFloor($(".floor.active").index() + ((event.deltaY > 0)? 1 : -1));
	});

	$("#page_overlay .cancel").click(function() {
		$("#page_overlay").removeClass("active");
	});
	$("#page_overlay .apply").click(function() {
		apply_for_room(this.dataset.room);
	});

}