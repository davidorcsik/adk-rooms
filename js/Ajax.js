let sessionToken;
var roomOccupancyRefreshTimer;
let residentToken = new URL(window.location.href).searchParams.get("token");

function beginSession() {
    $.get({
        url: "php/begin_session.php",
        data: {"residentToken": residentToken},
        contentType: "plain/text",
        success: function(data) {
            let tokenRegex = "[0-9A-f]{5,40}";
            let dataRegexMatchResult = data.match(tokenRegex);
            if (dataRegexMatchResult !== null && dataRegexMatchResult.length === 1) sessionToken = data;
        },
        error: function(xhr, status) {
            console.log(status);
            console.error("Could not acquire session token!");
        }
    }).done(function() {
        get_rooms_old_occupancy();
        get_rooms_occupancy();
        roomOccupancyRefreshTimer = setInterval(get_rooms_occupancy, 5000);
    });
}

function get_rooms_occupancy() {
    $.get({
        url: "php/list_rooms.php",
        data: {"sessionToken": sessionToken, "layout": "new"},
        contentType: "application/json",
        success: function(data) {
            parseOccupancy(JSON.parse(data));
        },
        error: function(xhr, status) {
            console.log(status);
            console.error("Could not query room occupancy!");
            alert("Hiba történt kérem próbálja meg később!");
            clearInterval(roomOccupancyRefreshTimer);
        }
    })
}

function get_rooms_old_occupancy() {
    $.get({
        url: "php/list_rooms.php",
        data: {"sessionToken": sessionToken, "layout": "old"},
        contentType: "application/json",
        success: function(data) {
            parseOldOccupancy(JSON.parse(data));
        },
        error: function(xhr, status) {
            console.log(status);
            console.error("Could not query old room occupancy!");
            alert("Hiba történt kérem próbálja meg később!");
            clearInterval(roomOccupancyRefreshTimer);
        }
    });
}

function get_user_info() {
    $.get({
        url: "php/get_user_info.php",
        data: {"residentToken": residentToken},
        contentType: "application/json",
        success: function(data) {
            console.log(data);
            let parsedData = JSON.parse(data);
            $("#user_info #name").html(parsedData.name);
            $("#user_info #neptun").html(parsedData.neptun_id);
            $("#user_info #current_room").html((parsedData.id != null)?parsedData.id : "-");
        },
        error: function(xhr, status) {
            console.log(status);
            console.error("Could not query old room occupancy!");
            alert("Hiba történt kérem próbálja meg később!");
            clearInterval(roomOccupancyRefreshTimer);
        }
    });
}

function apply_for_room(roomNumber) {
    $.get({
        url: "php/apply_for_room.php",
        data: {"sessionToken": sessionToken, "roomNumber": roomNumber},
        contentType: "application/json",
        success: function(data) {
            $("#apply_box").fadeOut("slow", function() {
                $("#notify_box p").html("Jelentkezés leadva!");
                $("#notify_box").fadeIn("slow");
                setTimeout(function() {
                    $("#page_overlay").removeClass("active");
                    $("#notify_box").css("display", "none");
                }, 1000);
            });
        },
        error: function(xhr, status) {
            console.log(status);
            console.error("Could not apply for room!");
            alert("Hiba történt kérem próbálja meg később!");
            clearInterval(roomOccupancyRefreshTimer);
        }
    }).done(function() {
        get_rooms_occupancy();
        get_user_info();
    });
}

function dismiss_room() {
    $.get({
        url: "php/apply_for_room.php",
        data: {"sessionToken": sessionToken},
        contentType: "application/json",
        success: function(data) {
            $("#page_overlay").addClass("active");
            $("#notify_box p").html("Kijelentkezve a szobából!");
            $("#notify_box").fadeIn("fast");
            setTimeout(function() {
                $("#page_overlay").removeClass("active");
                $("#notify_box").css("display", "none");
            }, 2000);
        },
        error: function(xhr, status) {
            console.log(status);
            console.error("Could not dismiss for room!");
            alert("Hiba történt kérem próbálja meg később!");
            clearInterval(roomOccupancyRefreshTimer);
        }
    }).done(function() {
        get_rooms_occupancy();
        get_user_info();
    });
}