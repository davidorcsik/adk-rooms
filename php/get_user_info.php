<?php
    require_once("db_config.php");
    require_once("session.php");

    if (!isset($_GET["residentToken"])) die("No token provided!");
    if (is_sha1($_GET["residentToken"]) !== 1) die("Invalid token!");

    $sql_handle = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    $sql_handle->set_charset("utf8");

    $resident_token_filtered = $sql_handle->real_escape_string($_GET["residentToken"]);

    $statement = $sql_handle->prepare("CALL get_user_info(?)");
    $statement->bind_param("s", $resident_token_filtered);

    $statement->execute();

    $result = $statement->get_result();
    if ($result->num_rows == 1) {
        $user_info_temp = $result->fetch_array(MYSQLI_ASSOC); //TODO: room on empty

        echo json_encode($user_info_temp, JSON_UNESCAPED_UNICODE);
    } else echo "Can't find user!";

    $sql_handle->close();