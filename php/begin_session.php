<?php
    require_once("session.php");
    if (!isset($_GET["residentToken"])) die("No token provided!");
    if (is_sha1($_GET["residentToken"]) !== 1) die("Invalid token!");
    echo generate_session_token($_GET["residentToken"]);