<?php
    require_once("db_config.php");
    require_once("session.php");

    if (!isset($_GET["sessionToken"])) die("No token provided!");
    if (is_sha1($_GET["sessionToken"]) !== 1 || !is_session_token_valid($_GET["sessionToken"])) die("Invalid token!");

    $sql_handle = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $room_number = $sql_handle->real_escape_string($_GET["roomNumber"]);

    $statement = $sql_handle->prepare("call apply_for_room(?, ?)");
    $statement->bind_param("ss", $_GET["sessionToken"], $room_number);
    $statement->execute();

    $result = $statement->get_result()->fetch_array(MYSQLI_NUM);
    echo $result[0];


    $result->free_result();
    $sql_handle->close();