<?php
    require_once("db_config.php");
    require_once("session.php");
    header('Content-Type: text/html; charset=utf-8');

    if (!isset($_GET["sessionToken"])) die("No token provided!");
    if (is_sha1($_GET["sessionToken"]) !== 1 || !is_session_token_valid($_GET["sessionToken"])) die("Invalid token!");

    if (!isset($_GET["layout"])) die("No layout id provided!");

    $query_string = "";
    if      ($_GET["layout"] == "new") $query_string = "CALL get_room_occupancy()";
    elseif  ($_GET["layout"] == "old") $query_string = "CALL get_room_old_occupancy()";
    else    die("Invalid layout id!");

    $sql_handle = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    $sql_handle->set_charset("utf8");

    $result = $sql_handle->query("call get_new_room_occupancy()") or die("query error");

    $result_array = array();
    while ($row = mysqli_fetch_assoc($result)) array_push($result_array, json_encode($row, JSON_UNESCAPED_UNICODE));
    $json = json_encode($result_array);

    $result->free_result();
    $sql_handle->close();

    $json = str_replace("\\\"", "\"", $json);
    $json = str_replace("\"{", "{", $json);
    $json = str_replace("}\"", "}", $json);
    $json = str_replace("false,", "", $json);
    $json = str_replace(",false]", "]", $json);

    echo $json;