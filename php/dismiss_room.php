<?php
    require_once("db_config.php");
    require_once("session.php");

    if (!isset($_GET["sessionToken"])) die("No token provided!");
    if (is_sha1($_GET["sessionToken"]) !== 1 || !is_session_token_valid($_GET["sessionToken"])) die("Invalid token!");

    $sql_handle = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $statement = $sql_handle->prepare("call dismiss_room(?)");
    $statement->bind_param("s", $_GET["sessionToken"]);
    $statement->execute();

    $sql_handle->close();