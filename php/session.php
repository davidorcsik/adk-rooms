<?php
    require("db_config.php");

    function is_sha1($str) {
        return preg_match_all("/[0-9A-f]{5,40}/", $str);
    }

    function is_session_token_valid($session_token) {
        $sql_handle = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        $session_token_filtered = $sql_handle->real_escape_string($session_token);

        $statement = $sql_handle->prepare("CALL is_session_token_valid(?)");
        $statement->bind_param("s", $session_token_filtered);

        $statement->execute();

        $result = $statement->get_result();
        $session_token_exists = $result->fetch_array(MYSQLI_NUM)[0];

        $statement->free_result();
        $sql_handle->close();

        return $session_token_exists == 1;
    }

    function generate_session_token($resident_token) {
        $sql_handle = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        $resident_token_filtered = $sql_handle->real_escape_string($resident_token);

        $statement = $sql_handle->prepare("CALL generate_session_token(?)");
        $statement->bind_param("s", $resident_token_filtered);

        $statement->execute();

        $result = $statement->get_result();
        $session_token = $result->fetch_array(MYSQLI_NUM)[0];

        $statement->free_result();
        $sql_handle->close();

        return $session_token;
    }
