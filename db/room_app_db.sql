-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 21, 2019 at 08:00 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `room_app_db`
--
CREATE DATABASE IF NOT EXISTS `room_app_db` DEFAULT CHARACTER SET utf16 COLLATE utf16_general_ci;
USE `room_app_db`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_session_token` (IN `resident_token` VARCHAR(100))  NO SQL
BEGIN
	SELECT `neptun_id` INTO @resident_neptun_id FROM `people` WHERE `token` = `resident_token`;

	SELECT CONVERT(FLOOR(rand()*100000), CHAR(5)) INTO @generated_salt;

	SELECT SHA1(CONCAT(@generated_salt, @resident_neptun_id)) INTO @generated_token;

	START TRANSACTION;
	
    DELETE FROM `sessions` WHERE `neptun_id` = @resident_neptun_id;

	INSERT INTO `sessions` (`neptun_id`, `salt`, `token`) VALUES (@resident_neptun_id, @generated_salt, @generated_token);
	
    COMMIT;
    
    SELECT @generated_token;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_old_room_occupancy` ()  NO SQL
BEGIN
	SELECT `rooms`.`level`, `rooms`.`room_number`, `people`.`name`, `people`.`newbie` FROM (rooms INNER JOIN old_layout ON rooms.id = old_layout.room_id) INNER JOIN people ON old_layout.resident_id = people.id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_room_occupancy` ()  BEGIN
	SELECT `rooms`.`level`, `rooms`.`room_number`, `people`.`name`, `people`.`newbie` FROM (rooms INNER JOIN old_layout ON rooms.id = old_layout.room_id) INNER JOIN people ON old_layout.resident_id = people.id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_info` (IN `resident_token` VARCHAR(100))  NO SQL
BEGIN
SELECT `people`.`name`, `people`.`neptun_id`, `rooms`.`id` FROM (rooms INNER JOIN old_layout ON rooms.id = old_layout.room_id) INNER JOIN people ON old_layout.resident_id = people.id WHERE `people`.`token` = `resident_token`;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `is_session_token_valid` (IN `session_token` VARCHAR(100))  NO SQL
SELECT count(`id`) = 1 from `sessions` where `token` = `session_token`$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `new_layout`
--

CREATE TABLE `new_layout` (
  `room_id` int(11) NOT NULL,
  `resident_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `old_layout`
--

CREATE TABLE `old_layout` (
  `room_id` int(11) NOT NULL,
  `resident_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

--
-- Dumping data for table `old_layout`
--

INSERT INTO `old_layout` (`room_id`, `resident_id`) VALUES
(305, 1),
(801, 2),
(213, 3),
(315, 4),
(709, 5),
(904, 6),
(609, 7),
(714, 8),
(806, 9),
(714, 10),
(802, 11),
(109, 12),
(511, 13),
(603, 14),
(208, 15),
(210, 16),
(214, 17),
(413, 18),
(513, 19),
(101, 20),
(912, 21),
(607, 22),
(704, 23),
(908, 24),
(814, 25),
(208, 26),
(403, 27),
(908, 28),
(302, 29),
(812, 30),
(711, 31),
(203, 32),
(405, 33),
(802, 34),
(710, 35),
(506, 36),
(212, 37),
(610, 38),
(911, 39),
(508, 40),
(113, 41),
(411, 42),
(707, 43),
(512, 44),
(506, 45),
(206, 46),
(807, 47),
(111, 48),
(405, 49),
(115, 50),
(808, 51),
(910, 52),
(701, 53),
(615, 54),
(308, 55),
(206, 56),
(913, 57),
(806, 58),
(507, 59),
(114, 60),
(602, 61),
(601, 62),
(306, 63),
(705, 64),
(402, 65),
(710, 66),
(307, 67),
(301, 68),
(810, 69),
(303, 70),
(215, 71),
(914, 72),
(713, 73),
(708, 74),
(611, 75),
(915, 76),
(905, 77),
(502, 78),
(901, 79),
(811, 80),
(102, 81),
(210, 82),
(108, 83),
(812, 84),
(205, 85),
(1, 86),
(713, 87),
(401, 88),
(312, 89),
(612, 90),
(303, 91),
(414, 92),
(914, 93),
(505, 94),
(203, 95),
(608, 96),
(207, 97),
(805, 98),
(503, 99),
(409, 100),
(507, 101),
(609, 102),
(204, 103),
(314, 104),
(712, 105),
(204, 106),
(913, 107),
(304, 108),
(613, 109),
(803, 110),
(809, 111),
(906, 112),
(803, 113),
(5, 114),
(604, 115),
(909, 116),
(406, 117),
(907, 118),
(906, 119),
(309, 120),
(807, 121),
(313, 122),
(308, 123),
(705, 124),
(602, 125),
(408, 126),
(804, 127),
(515, 128),
(310, 129),
(606, 130),
(511, 131),
(302, 132),
(501, 133),
(407, 134),
(902, 135),
(613, 136),
(702, 137),
(715, 138),
(503, 139),
(706, 140),
(404, 141),
(808, 142),
(903, 143),
(612, 144),
(102, 145),
(305, 146),
(715, 147),
(209, 148),
(310, 149),
(412, 150),
(514, 151),
(502, 152),
(215, 153),
(703, 154),
(113, 155),
(2, 156),
(110, 157),
(901, 158),
(512, 159),
(312, 160),
(115, 161),
(607, 162),
(404, 163),
(813, 164),
(702, 165),
(212, 166),
(804, 167),
(314, 168),
(605, 169),
(708, 170),
(402, 171),
(403, 172),
(509, 173),
(909, 174),
(709, 175),
(509, 176),
(610, 177),
(315, 178),
(712, 179),
(810, 180),
(703, 181),
(608, 182),
(114, 183),
(907, 184),
(811, 185),
(706, 186),
(109, 187),
(304, 188),
(611, 189),
(411, 190),
(513, 191),
(406, 192),
(307, 193),
(4, 194),
(903, 195),
(415, 196),
(911, 197),
(508, 198),
(912, 199),
(614, 200),
(201, 201),
(407, 202),
(207, 203),
(904, 204),
(409, 205),
(814, 206),
(401, 207),
(805, 208),
(209, 209),
(6, 210),
(205, 211),
(410, 212),
(603, 213),
(6, 214),
(202, 215),
(4, 216),
(110, 217),
(213, 218),
(414, 219),
(813, 220),
(505, 221),
(306, 222),
(413, 223),
(915, 224),
(515, 225),
(905, 226),
(707, 227),
(311, 228),
(604, 229),
(910, 230),
(202, 231),
(214, 232),
(311, 233),
(101, 234),
(211, 235),
(201, 236),
(809, 237),
(701, 238),
(5, 239),
(313, 240),
(111, 241),
(504, 242),
(410, 243),
(601, 244),
(415, 245),
(514, 246),
(504, 247),
(211, 248),
(801, 249),
(704, 250),
(408, 251),
(301, 252),
(902, 253),
(606, 254),
(2, 255),
(615, 256),
(605, 257);

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `neptun_id` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `newbie` tinyint(1) NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `name`, `neptun_id`, `email`, `newbie`, `token`) VALUES
(1, 'Széles Bence', 'ABFUBF', 'szelesbence1997@citromail.hu', 0, '8bb1329f62a021cec4ca856fb4d1d1acb876619f'),
(2, 'Nagy Laura', 'AD0BDO', 'nagylaura.elte@gmail.com', 0, 'c94f066d28d9d2860dfca727830de8375a2aa8aa'),
(3, 'Kiss Tibor', 'AFLXQ5', 'tibor.kiss.1999@gmail.com', 0, '8e0b3050660d424e31a414667d9ff36e13b419d4'),
(4, 'Merzay Csenge Laura', 'AF5D8M', 'merzaycs@gmail.com', 0, 'a7281aed53bc41c55f8c7f1f86cb02eb30dce62a'),
(5, 'Molnár Gyöngyi', 'AG8SCJ', 'm.gyongyi88@gmail.com', 0, 'ce114a0da51d9777b54ebb44cb8921f1fba007e3'),
(6, 'Harmat Fruzsina', 'ALZ450', 'harmat.fruzsina@gmail.com', 0, 'b76f241708e016e58eb01b697e81dc116ab9c479'),
(7, 'Popovics Rebeka Orsolya', 'AOFU84', 'rebeka.popovics@gmail.com', 0, '1d9fc86d99abce10a32af32e4ab9cae3f8123717'),
(8, 'Kis Zsanett', 'AVHKQ0', 'zsanettkis98@gmail.com', 0, '1acdf17e5e7119b7f2b3ce3c37c6b80a533d3903'),
(9, 'Cziczeri Klaudia', 'AZ19EY', 'klaudiacziczeri@gmail.com', 0, 'bfae8fcfd89ea927a678e131a6fa0d9d9e9c93b2'),
(10, 'Horváth Lívia', 'A35YWJ', 'h.livia05@gmail.com', 0, 'ef90cf78652693168211550fb5f9ce5597f8f015'),
(11, 'Szita Bettina', 'A5T634', 'szitabettina@gmail.com', 0, 'c346cb22ebc7f568c9ffa44ddd0e36c2d98c61d1'),
(12, 'Kustár Lídia', 'A6JKND', 'kustarlidia@gmail.com', 0, 'e337bdb1acbd9500eeb8b99dc7827a2ee42fde7b'),
(13, 'Gál Huba', 'A8GBYL', 'hubaize@gmail.com', 0, 'b4ba6bf24a09bf7ded3c65aeb74803114ae9ecb9'),
(14, 'Kellner Miklós Péter', 'A8N7ET', 'mikloskellner98@gmail.com', 0, '0eb96e01625a94ba00f343d3a5622174e0324d29'),
(15, 'Pocsaji Emese', 'BCTNHX', 'bogidepp@gmail.com', 0, 'c2341908a50e3c65a884ce0e2360278c370e466e'),
(16, 'Fésű Anna Magdolna', 'BDAKF1', 'f.annamagdolna@gmail.com', 0, '2a2d31a3944c24193f859c4c9b3131870ba345c8'),
(17, 'Nándorhegyi Krisztina', 'BIRU46', 'krisztinanandorhegyi@gmail.com', 0, 'f790e5dee5f8932f08bb9250bab74447c9067737'),
(18, 'László Anikó', 'BJ1CF9', 'laszlo.aniko1994@gmail.com', 0, '19dbee6ab17f7172cc2765c638a973c0e7ae5401'),
(19, 'Hay Anna Klára', 'BJ5YQ3', 'hanna1998@hotmail.hu', 0, 'b0910ca03ff8d10b294bfe3a3df7c566c6c73b65'),
(20, 'Jberi Ep El Gaaloul Ghazoua', 'BKYMAW', 'ghazouajberi@outlook.fr', 0, '8a43ffecaf234faa630057af27d21c41dada0321'),
(21, 'Bónácz Judit', 'BKZV6L', 'ballarej@gmail.com', 0, '7ca9ce4457ac2d12842d1ab5bb98ad72d8d9ba32'),
(22, 'Áldorfai Gergő', 'BM2WWJ', 'gergoaldorfai@gmail.com', 0, '507982a4b82c45d2c350b1f98b71fff57bbfb1e5'),
(23, 'Papp Martin', 'BOVAZC', 'pappmartin07@gmail.com', 0, '4e6ef9eefd93a75f947bc77a0e424613f19299dd'),
(24, 'Hajcsák Gábor', 'BOYR6B', 'hajcsak.gabor@gmail.com', 0, '48f1921b8698955ea1a3e41a1615ef16a378e6e2'),
(25, 'Mészáros Gergely Marcell', 'BTV520', 'geri98@gmail.com', 0, 'e54fbc95c8bb8d4db49d03b2bf045713593d1bfc'),
(26, 'Négyökrű Zsófia', 'BVKEGG', 'negyokru.zsofia@gmail.com', 0, 'aa36cebeb670259c3d27c691357308b30cb73dc8'),
(27, 'Barabás Sándor Alex', 'B1G8PM', 'barabas.alex@outlook.hu', 0, 'b8c645283f36e3554637ee83d65caba044ab4da1'),
(28, 'Czáboczky Szabolcs', 'B4W321', 'czaboczky@gmail.com', 0, 'bf4b170b4c327dcdb76f681dc7e16fe3bcaea7ae'),
(29, 'Somlai Luca Petra', 'CBH4GP', 'lucasomlai@icloud.com', 0, 'a1aef0a8e748f02594e8bd5939e848cdb08298ad'),
(30, 'Dörögdi József', 'CC6D53', 'dorogdijosti@gmail.com', 0, '8750ec5119498724c50e38eaed5ee280e925fef8'),
(31, 'Pongrácz Luca', 'CFPYQ3', 'pongraczluca5@gmail.com', 0, 'ec4e20be4013d639b7647bcd66f46ed4118b13b4'),
(32, 'Szabó Noémi', 'CHFDFV', 'noemisza292@gmail.com', 0, 'a71d9bbe1349656c534b55518aff6b3047b1eaf1'),
(33, 'Németh Viola', 'CKG841', 'nemethviola@hotmail.com', 0, '22c41fd921d834aa29c76f52abfde810c0a16370'),
(34, 'Kis Virág Tamara', 'CMH5EC', 'kv.tamara@outlook.com', 0, 'fd5a936ecbbb4e1c70cc5e29dabd197542f0b625'),
(35, 'Hammer Angéla Márta', 'CMW8GC', 'angela.hammer97@gmail.com', 0, 'abb5ed9d1b4da9d8567369ec6e02ba9733988c98'),
(36, 'Szabó Martin', 'CQBO0I', 'martin77szabo@gmail.com', 0, 'dfabafdae7b7d4b0ef9741f80eb66d101760e6b4'),
(37, 'Pivoda Tomás', 'CUPW9T', 'pivodatamas@gmail.com', 0, '1ca475ce0c4699f0e24cee1d08587c376ccf1be2'),
(38, 'Andó Miklós Álmos', 'CUT9B1', 'ando.miklos@gmail.hu', 0, '65a847a6d9ca19197b2ec6a43322fb992e58dfea'),
(39, 'Kiss Ágota', 'CWO6T1', 'kissagota95@gmail.com', 0, 'a05625ac2ce36783ea3fdb605879b9bc6a5ef866'),
(40, 'Papp Viktor', 'CXDA3J', 'viktorpapp14@gmail.com', 0, '59871097e086fe4c120bb2a9f4e358e25c67961c'),
(41, 'Szeitz Lili', 'CZ4IDV', 'lillyke95@gmail.com', 0, 'f5f70440707dc610fc2a21ad0102c0562d147414'),
(42, 'Ellenrieder Mira', 'C0NXY8', 'mirannaberta@gmail.com', 0, '46dcd6aa42aca5c44b32e164afcb467ae76264ad'),
(43, 'Fekete Ferenc', 'C7KUKO', 'skorpioqq@freemail.hu', 0, '5941acb4768b035286d4c7b33be7f35b5579b8a7'),
(44, 'Kelemen Kamilla', 'DKTQJR', 'kami.kelemen@gmail.com', 0, '7832ecc053edc7ef35383cac630b518015125baa'),
(45, 'Dézsi László', 'DM7UYJ', 'laszlo.d0730@gmail.com', 0, '892e1d4c6be9f436a3dc9d109404649109290c09'),
(46, 'Tátrai Áron', 'DNP9CO', 'tatrai.aron@hotmail.com', 0, 'e9a99ef45287975e8e6f2b52513d40fe7a2b1faa'),
(47, 'Szarka Roland', 'DPL880', 'lightroli7@gmail.com', 0, '6d6349300060a6a95ee15f93219b6a02451bebda'),
(48, 'Végh Márk', 'DP2NAS', 'markivegh@gmail.com', 0, 'dfc2cb9be3df2af0eea9e25426eecf9ffffd94b0'),
(49, 'Szabó Csanád', 'DP5TDE', 'szabo.csanad97@gmail.com', 0, '74685855fbd10507acdc0ee30ecc72500b7da42c'),
(50, 'Perez Jimenez Mario', 'DSGY78', 'marioperez@ciencias.unam.mx', 0, 'd26bb43ea6543ecc23dc230ffb4b7a32bf25cb9a'),
(51, 'Pap Anna Gyöngyi', 'DUBDB3', 'anna_gyongyi.pap@hotmail.com', 0, '4ea6728a5db725509e38932837660b849bed3978'),
(52, 'Csáki József Máté', 'DUPEEK', 'csaki.mate@outlook.com', 0, '550e50f5b7596924917e683c721496c6b01938d0'),
(53, 'Cziráki Bálint', 'DU9B93', 'kinizsi34@freemail.hu', 0, '1bcb6ed97f4c16891ddf24acafa0e2608a3c7887'),
(54, 'Patai Rebeka', 'DYWI3A', 'rebpat002@gmail.com', 0, '8f7cc5588ba9d50238f11aad2c6d2b13f9daa30c'),
(55, 'Szabó Levente', 'DZHAUL', 'levevo14@gmail.com', 0, '359a893b1909d106a8c5fc5d0a91a61dcf9b298f'),
(56, 'Beckl Martin', 'D564O6', 'bcklmrtn@gmail.com', 0, '65559a8fa97dd40f70fa938ca729acec22dada1a'),
(57, 'Szabó Éva', 'D7927N', 'gagarinalba@gmail.com', 0, '8d2db87c84d9b30fe79759195f127f3ffe0e1c69'),
(58, 'Lampert Jácinta', 'EAML63', 'jacinta.lampert@gmail.com', 0, '84c5f948c418198efc235279af531c0c0322b427'),
(59, 'Bujdosó Rita', 'EFV0EP', 'krashavik1@gmail.com', 0, 'a65d27303b0b567e65f70d440a0814cd7e8ef249'),
(60, 'Dömötör Rebeka', 'EJNXTK', 'rebecca.domotor@gmail.com', 0, '836ad3a6e7b3a254c186960c9068342606b51e51'),
(61, 'Szirmai Kitti', 'EK709V', 'szirmaikitti@gmail.com', 0, '50539c32a7cdaa25e65fa12303f7a9a0aca54d11'),
(62, 'Kormos Kevin', 'ELHYQA', 'kormoskevin@gmail.com', 0, '7ef05ffb91d6263a0d819d14626056cac975a068'),
(63, 'Marczel Krisztina', 'EM9LN5', 'marczelkriszta@gmail.com', 0, 'a6d830146499381a7646467dbbcdbb6e9759878f'),
(64, 'Szabó Noémi', 'EOYMI9', 'szabo.noemi60@gmail.com', 0, '98445fee4015bb159908fde394731869b86efaf8'),
(65, 'Csatlós Kata', 'ER40IG', 'cskata110@gmail.com', 0, 'd41225ea79d03267304c0b0db94638a06a35a8d3'),
(66, 'Brezóczki Bianka', 'ER8SLJ', 'biankabrezoczki@gmail.com', 0, '25fb501a8f3c5c10d2fe7e2e64f429952c2fb03f'),
(67, 'Bihari Lion Sebastian', 'EVPZI1', 'Sebilion@hotmail.ca', 0, 'dae162e2434f3a8b012e7cc1eb0612831ee38ffa'),
(68, 'Fekete Csilla', 'EV9BI6', 'csilla.fekete0302@gmail.com', 0, 'fa4376bc1686c82cca883da9a45e5f0d116e8dc1'),
(69, 'Berta András', 'EXBVD4', 'b.andris97@gmail.com', 0, 'a0e731915b975fbe86c0e0260151159f51f9ef83'),
(70, 'Balla Nikolett', 'EXCHFD', 'nikiballa@gmail.com', 0, '43c50e7423bba8403734593d1a23dfa5fb7afed8'),
(71, 'Lelkes Csilla', 'EXMQ30', 'lelkescsilla7@gmail.com', 0, '5f1eb66afe8dc758f3d6b5ee0250969bb40bce73'),
(72, 'Zádori Anna', 'EZN2K5', 'zadoriancsi@live.com', 0, '30b96b148502fc9ef3a0963940594fd84aa2a728'),
(73, 'Dulai Dóra', 'E7249B', 'dulai.doora@gmail.com', 0, 'aaf5fa9efcbc5e96dbc78213d4d9254f07c3da4f'),
(74, 'Eibel András', 'E87U2S', 'andraseibel@gmail.com', 0, '012a67acc59a23660727e79e6526eeb18d0f2c65'),
(75, 'Hunyadi Ákos', 'FEO859', 'hunyadiak12@gmail.com', 0, '141c3da4b342b9a1cd0d6df54e8512c13824fde8'),
(76, 'Hunyadkürti Soma', 'FFSBWZ', 'soma.hunyadkurti@gmail.com', 0, '529a51e363e21b09bacfe2335c2450d4b10d8c7a'),
(77, 'Földesi Ákos', 'FHT8QL', 'foldesiakos1@gmail.com', 0, '2e41fe58c5ef1ba98620934d802b03936b7c3a9c'),
(78, 'Zsibrita Vera', 'FJEDS3', 'zsibrita.v@gmail.com', 0, '7767a09d9f50f35920e2d5aed0ccc601863709bc'),
(79, 'Jelenik Dorka', 'FJJXC1', 'dorka.jelenik@freemail.hu', 0, 'b6a80c7d597eb81d65afdd804810f313f48f8e63'),
(80, 'Sipos Tamás', 'FKDCR3', 'sipos.tommi@gmail.com', 0, 'dc02368bec91c813a866a0d4b98f9c0db44ee389'),
(81, 'Boulassel Soufiane', 'FNFH40', 'soufjan@caesar.elte.hu', 0, '45a39a3ef78b05ef286179aa66443c21138f0950'),
(82, 'Varga Ádám Zoltán', 'FSRRSU', 'adamvarga2013@gmail.com', 0, '35f851668ce303180b2c9e22c09fb71ccd80273e'),
(83, 'Brutóczki András', 'FUSIQG', 'andrisbruti@gmail.com', 0, '5dc916ad90e87a909090994b9d2387ef3437d077'),
(84, 'Tóth Zoltán', 'FXILY6', 'tothzol27@gmail.com', 0, '1cebfc006b8aa4d00d9c44d25fa23e44fe00d0db'),
(85, 'Pásztor Alexandra Katalin', 'F0RV01', 'alexa.sissy@gmail.com', 0, '17cd01a6d5509de0418ff611af2153517174d1a5'),
(86, 'Erdenebat Odbayar', 'F66XH5', 'mitsu.odii@yahoo.com', 0, 'aa03c96b7ae406c8bc7e4a2b5978766d8cd5082c'),
(87, 'Hegedüsová Angelika', 'F7CW9L', 'asangiu2@gmail.com', 0, '48160c753d368eae3e04e326968edf2f5b77902d'),
(88, 'Juhász Erika Mária', 'F78957', 'juhasz.erika43@gmail.com', 0, 'ad5773810b3ea69c88711b67d9e62ba3b418e608'),
(89, 'Somogyi Rebeka', 'F90N7D', 'somogyirebeka@gmail.com', 0, '832bf843632c2ffe88611207854aa64727232465'),
(90, 'Mentes Laura', 'GA5C9J', 'laura.mentes97@gmail.com', 0, '7596c311c35142d78ff1804144c465fffa664709'),
(91, 'Mészáros Emma', 'GC3V7A', '03emma11@gmail.com', 0, '568070e3bf270cbffbaacc9cd5e7c910035141f0'),
(92, 'Horváth Emese', 'GED6DW', 'little.sunns@gmail.com', 0, 'fe22dff9797e9cf853599b490652b0e6da8788fc'),
(93, 'Chen Meng Jia', 'GJDMRI', 'csacsa.meng@gmail.com', 0, 'df9a04bbcf1df1cc31048205efaa708052e103e8'),
(94, 'Lengyel Angelika', 'GJI8YO', 'langelika96@gmail.com', 0, 'ed9a6549deb541731113d0a75d0911a6fdeba452'),
(95, 'Tóth Petra Eliza', 'GPZIH2', 'tothpetraeliza@gmail.com', 0, '86cfb9c91025ebb62bbb9d17b3e62014ae55966c'),
(96, 'Kertész Ákos', 'GT7IVZ', 'kerteszakos0@gmail.com', 0, '2b70054baf05ff37815f591d8ad4ecdf35e17f36'),
(97, 'Berta Zsófia', 'G6LNXG', 'berta.zsofi97@gmail.com', 0, 'a51e0d72c9c01e21b3d89a7c4f824c947d64ea59'),
(98, 'Pásztor Eszter', 'HA8IWL', 'epasztor.119@gmail.com', 0, 'df416d2c3e2adf7b22a2ef0cf1fb922514623a41'),
(99, 'Rabb Dorottya', 'HHKJ33', 'dori7798@gmail.com', 0, 'a0a3ce3f27392aab47359855908e7f7b901d103f'),
(100, 'Zsemberi Fiametta', 'HMPZ3Q', 'zsemberi.metti@gmail.com', 0, 'f6a132e9ff41194b5edc719f81afd0b9b5db76f9'),
(101, 'Lakatos Fruzsina', 'HMZ0Y6', 'lakatos.fruzsina934@gmail.com', 0, '727c0ba1d9db1b28803bdc85b5d3d304658241c5'),
(102, 'Ujj Kíra Diána', 'HQBMCT', 'ujjkira@gmail.com', 0, 'a91c4890a947daf1256627a1c82b04a1ac4c13f2'),
(103, 'Ébner Anna', 'HQM3WA', 'ebneranna96@gmail.com', 0, '821b1f4936ebb679291118541e63aa4a3b89a38d'),
(104, 'Szulimán Gergely', 'HRAXTK', 'szuliman.gergely@gmail.com', 0, '8a57c422fa8dcffcc78d87241a0535eddedcc349'),
(105, 'Bodó Laura', 'HSJR5J', 'bodolaura007@gmail.com', 0, '2cea9c95ed2bbf7826cae5bbcd6ec4e529231707'),
(106, 'Ébner Zsuzsa', 'HTPUXK', 'ebnerzsuzsa@gmail.com', 0, 'ea08cf99b1c2be8aae2516f4dad4dfd376b7de06'),
(107, 'Markó Ádám', 'H6VS54', 'marqadam@gmail.com', 0, '6e4a145ddf3632edd277014292d89eade3f51bd1'),
(108, 'Varga Emese', 'H6Y2HO', 'emesevarga86@gmail.com', 0, 'fb84d4031c6763e074726c6ed248fe4293c2ef61'),
(109, 'Andrejcsik Enikő', 'H9JOQL', 'andrejcsikeniko@gmail.com', 0, '627da42325116bd5335b06a4099d2d7667088d78'),
(110, 'Pászti-Nagy Dorottya', 'H9RORC', 'pndottie@gmail.com', 0, '404c8174a20970b4e360466bc36c715b40d78446'),
(111, 'Oláh Eszter Etelka', 'ICPVFZ', 'eszterlh3@gmail.com', 0, '38ac9253d6f424cf2249ef68817e2459cba6b5bd'),
(112, 'Fehér András', 'IKDJIJ', 'feherandras@gmail.hu', 0, '3e1e9a84944b91c232d35a9e746eca67bcf30b82'),
(113, 'Szabó Fanni', 'INGECX', 'fanni977@freemail.hu', 0, 'e79f2dbebb7b2720b64e6d91d6da0f3d41ed9e4c'),
(114, 'Szijj Regina', 'IQREEO', 'szijjregina99710@gmail.com', 0, '4d6103559a1e4db3a294045caa3b8c5372d2068b'),
(115, 'Bálintová Barbora', 'ITH1WC', 'balint.barbara97@gmail.com', 0, '2973597dbc0c43ca460c2275ad98a049d4cf25b5'),
(116, 'Vecserek Andrea', 'IUYLB8', 'vecserekandika@gmail.com', 0, '6a6d24fa7ecfa7ffbfc50419961ca8882f88d3ee'),
(117, 'Uhrin Balázs', 'IWKCGW', 'balauhr@freemail.hu', 0, '4cc7a86e607db930984f4b8b79ba202c0d115436'),
(118, 'Hambuch Júlia Anna', 'IY6H5A', 'julia.anna.hambuch@gmail.com', 0, '6256a018c325468a5ac5e7041d1b370992584682'),
(119, 'Pass Natália Leonóra', 'IZZ7ZN', 'arkhamasylum2.0@gmail.com', 0, '184e708dca92a8524ff9a6a86190c463460489c5'),
(120, 'Morvai Nóra', 'I153PO', 'morvainora@gmail.com', 0, 'f67214f17f279cc385ce5a94b6345d8e8e751c8a'),
(121, 'Török Ábel', 'I1643Y', 'torokabel@gmail.com', 0, 'f1750d0fc437a81738671c50b86187945ac8a0ea'),
(122, 'Bús Bence', 'I58R8T', '9busbence99@gmail.com', 0, '7df2957ef5c2e9d3f4a79f12655d0f87c6a57501'),
(123, 'Kriston Bálint', 'I7MV70', 'kriston.balint@gmail.com', 0, 'b7ba5d7ac865cefd41c5f56db3deb91895ea4454'),
(124, 'Mudry Anna Dorottya', 'I8L87G', 'mudrydorka@gmail.com', 0, '3468bcf64b0d02ec028575f15c0e146f8bad146c'),
(125, 'Popgeorgiev Kirill', 'I9WJ4A', 'popgeorgievkiril@gmail.com', 0, '8c3fcb4962d336fc6674c680833e1c33cf99a630'),
(126, 'Boris Vanessza', 'JCYZTW', 'borisvancsi@hotmail.com', 0, 'dc44ce30b936cb69d7c4f9313275e66142ac138d'),
(127, 'Retek Karina', 'JDQ617', 'retek.karina@gmail.com', 0, '2d737026084671df5b6cf8ee91997c8ccd52c2e9'),
(128, 'Polena Péter', 'JFAL5A', 'PPolena92@gmail.com', 0, 'fef31875eec26a0daa4713a660665de6242b729d'),
(129, 'Hősi Ramóna', 'JH1QS3', 'hosirami@hotmail.com', 0, '3b9efe4086a81bffdcbfd5813e10f233ba4b614b'),
(130, 'Farkas Eszter', 'JMDAOV', 'fesztike07@gmail.com', 0, '6e9d29d6c59269ef614d4bafbebbc4b37e716025'),
(131, 'Szűcs Ákos Gergő', 'JOX3W6', 'szucs.akos.gergo@gmail.com', 0, '0666678bf586e01f7c5814d9a1051a0e2ac79d54'),
(132, 'Fábián Ágnes', 'JP0FLE', 'fagca98@gmail.com', 0, '7d4ac51c49f01d94834b74573bd1eb429a675c82'),
(133, 'Simon Krisztián', 'JQGVUO', 'skrisztian96@outlook.hu', 0, '668389ce62fd702bf5bb92664156407d1616d1a1'),
(134, 'Nagy Mátyás', 'JRV9G7', 'nagymatyi96@gmail.com', 0, '1c4b152e038965b8a9ba6ce6299f5fd972124a5a'),
(135, 'Mrázik István', 'J1CQGP', 'mrazik.istvan@gmail.com', 0, '7b7c2b3252d45e3325305eca21fc803a8852377d'),
(136, 'Oláh Noémi Irén', 'J177RE', 'olah_noemi@yahoo.com', 0, 'ae61fdaaf0624c466a05cba9d76de001c40a9999'),
(137, 'Somogyi Dávid', 'KJSRAD', 'somogyidavid9829@gmail.com', 0, '65de1afd2d8c691e2ea31b793e00d17bebbd2aa8'),
(138, 'Kiszin Kitti', 'KNJR9H', 'kiszinkitti@gmail.com', 0, 'cdaf621ff4c942dca6a4c2c830cdd0f9ca53feb7'),
(139, 'Lakatos Alexandra', 'KRYMEI', 'alexandra.lakatos8881995@gmail.c', 0, '921a934e4ebc6c6008607c7af6a71da79e837a6f'),
(140, 'Szepesi Dávid', 'K0N228', 'forzeferrari@gmail.com', 0, 'cbd9e53e28b504432555b1c4c69f907c21968b6d'),
(141, 'Tózer Kitti', 'K6CBV9', 'kittitozer@gmail.com', 0, '0fe80fa80b9cfb6c966670dcef408e40801e45e6'),
(142, 'Kovács Beáta', 'LKP1PW', 'kovacsb1991@gmail.com', 0, '60f30a81bbfd4bc6c8b25752ddbc1382cf2c4760'),
(143, 'Kustos Balázs', 'LQX5J9', 'qstos.b@gmail.com', 0, '6e810dc89db46b3d3b745afca831d424fb7b37dc'),
(144, 'Henn Sofia', 'L45ACM', 'sofihenn@gmail.com', 0, 'ad6929e33d3e738069ca2b6930cf93e6d886b688'),
(145, 'Dridi Yahya', 'L8B50F', 'yahya.safe7@live.com', 0, 'f3f6631f776514b90b16aad91d0ff0b1d9292ca1'),
(146, 'Kázsmér Dávid', 'MH7AOT', 'kazsmerd@gmail.com', 0, 'a49c6ce5018268f843257c2306beeea799fff3cf'),
(147, 'Zélity Luca', 'MI2DXI', 'lucazelity99@gmail.com', 0, '8bd603125b929d46bc4eed8b822e67bc938fda36'),
(148, 'Kolárszki Dóra', 'MNE0JF', 'kolarszki.d@gmail.com', 0, 'a046014ae01af3294ef6402451cedcc5ed884f54'),
(149, 'Samulka Patricia Erika', 'MN6L20', 'samulkap@gmail.com', 0, 'f72447d3704f5d137bcb3a32ebd329b3332b4bad'),
(150, 'Csordás Bence', 'MSS4Z7', 'bencecsordas@gmail.com', 0, 'bff3671bd032ea3dc13ff1f625537c6ce5f2d85c'),
(151, 'Seres Zoltán', 'M0AUGI', 'seresesperes@gmail.com', 0, '19f0f359c8016d136419ad7a746546c6e5dc44dc'),
(152, 'Módos Barbara', 'M1O27L', 'modosbarbara@gmail.com', 0, '4760317933f30b3f18c512d819d1638e63251568'),
(153, 'Horváth Viktória', 'M17OBS', 'ho.viki0102@gmail.com', 0, '633745d505e1be742ca01efd241cab3938a73126'),
(154, 'Vigh Szilvia Ágnes', 'M5ZA44', 'vighagika@gmail.com', 0, '646b8bdf659df604597962b92304da4fe2e4b51c'),
(155, 'Máthé Luca', 'NG1AHJ', 'matheluca@gmail.com', 0, 'f21d9afcada3bd7099dfc8d2180c6113624a043c'),
(156, 'Török Bálint', 'NHOVTM', 'tb.hadvezer@gmail.com', 0, '8b611f4e40ad2ee1c985dc3db95ffb6abc7ae824'),
(157, 'Kovács Kincső Lilla', 'NXW4T1', 'kovacs.kincso909@gmail.com', 0, '66f8a18c29028c6785921c5fe612e006fa548898'),
(158, 'Nemesi Zsófia Kató', 'N1DZOD', 'iszkiri.deizibe@gmail.com', 0, 'db7d583935e09872456ec3c32fe5352f3366f86d'),
(159, 'Kis Marcell', 'N2BG1M', 'kismarcell95@gmail.com', 0, '8cad38ce18f6602b3a2c564e5a19498d569e6cd7'),
(160, 'Farkas Márta Brigitta', 'N7K7AM', 'farkasmartabrigitta@gmail.com', 0, 'c5fc6479f8b64d7019aa92c0e3856f7fd080401f'),
(161, 'Mendoza Cadena Lydia Mirabel', 'OACXRS', 'lmmendoza@ciencias.unam.mx', 0, '1096adf8062ff9da32ff711ae30b8d7acaafc672'),
(162, 'Gál Soma', 'OITAY3', 'kavics004@gmail.com', 0, '9ddcec1fd2e565d9d68d11e9f7dab76bdccf820a'),
(163, 'Perlaki Adriána', 'OPFVT9', 'peki96@hotmail.hu', 0, 'acbd0fc9f89ea4754d7f1dd9eaf2eaf493cd8745'),
(164, 'Sipos Jácint Zoltán', 'OR0S2G', 'saturnusfox@gmail.com', 0, 'c30c3fe507f93f3c114cf3fd8a3d90f7d8e0a2fc'),
(165, 'Balogh András', 'OSKQOO', 'bandras97@citromail.hu', 0, 'fd1dc4cb5cb0b4b674b903ae7913c3d69235904f'),
(166, 'Tóth Erik', 'OSVOTR', 'erik.toth97@gmail.com', 0, '62f6feb62282dc09675b3c640efab08e836fe0b5'),
(167, 'Skita Diána', 'OT8658', 'skitadina@gmail.com', 0, '213a4600c08e0e12ad5af1a8ffc20fd896bb9e07'),
(168, 'Prihoda István', 'OUB3KP', 'prihodaistvan@freemail.hu', 0, '7d47a578d10619ba13a9a872778286c80dc94ea8'),
(169, 'Gacsal Alexandra', 'OUKYOJ', 'gacsal96@citromail.hu', 0, 'b75f4647e59e10f896e6f1ae0e9a407cef77bec5'),
(170, 'Karóczkai Balázs', 'OUWGCR', 'karoczkaib@freemail.hu', 0, 'cac7027bf0f03466af2911d9d561a6304924538e'),
(171, 'Czakó Veronika Tímea', 'O675J6', 'czako.v.timea@gmail.com', 0, '14d61adb901d8a7572afdef030e7ff77499d2c05'),
(172, 'Gróf Olivér', 'O69AFF', 'grof.oliver94@gmail.com', 0, '0f5fbb5a8e8b73f1dca6a67f1b944824f2bcd8cd'),
(173, 'Varga Kata', 'O8OABR', 'varga.kata.b@gmail.com', 0, '6bbf1bc3802732bfcebf0a524be434630df6c4fd'),
(174, 'Horváth Tamara', 'PDF4VK', 'horvathtamaramika@gmail.com', 0, 'e8077065b331ae42bbe5e2af0fc53f154b4b882e'),
(175, 'Lanczki Regina', 'PF2Z17', 'lanczki.regina@gmail.com', 0, '776c0583dae1e89eff6fdff516273e6a902097af'),
(176, 'Kolozsi Blanka Irén', 'PHULGJ', 'kolozsi.blanka378@gmail.com', 0, '8acbe8a2a3647015e0f9789b3e6579b32342e53b'),
(177, 'Hunyadi Sándor', 'PKIG5R', 'hunyadi.sanyi93@gmail.com', 0, '4c3635c6a3e3f231bbe4cf87445b9fb5db66faf7'),
(178, 'Berecz Lilla Mária', 'PKII74', 'lillaberecz37@gmail.com', 0, '0cb9af3705d6fa1f71860529d5dc1eed89f1a53d'),
(179, 'Huszár Dorina', 'PW9IY5', 'huszardorcsi@gmail.com', 0, 'cb401cb8f462bdbdb5ccbc6b7e4e119c3443a8fd'),
(180, 'Tóth Dávid Márk', 'P4I6R6', 'tothdave8@gmail.com', 0, '699c24a75f961fdf7680745236df730a34dec5ef'),
(181, 'Szabó Fanni', 'P5B3KA', 'szabofannika20@gmail.com', 0, 'caecd12bc336ddd04ab7be2a94c3eee78a076ff4'),
(182, 'Kun Fanni', 'P9C4PO', 'fanni.felisa@gmail.com', 0, '894582a832b44188bb36e247f5c343bdeb8e9497'),
(183, 'Dömény Bianka', 'QAZ595', 'd.bia.cat@hotmail.com', 0, 'a992f14d094a848fed3858c4c0ba646518a028f2'),
(184, 'Szalai Kinga', 'QCOGOG', 'szalaikinga15@gmail.com', 0, '40e8a35e711a4407c264d4f0a450c4e87cde6368'),
(185, 'Kis Iván', 'QCWLTR', 'kisivan18@gmail.com', 0, 'ae790b5936dc2e6bb0591582149fc455e5b3a184'),
(186, 'Rucska Ferenc', 'QCZXXR', 'francseszko11@gmail.com', 0, '1055a665077a89ab356e21e9436ec2e49948faee'),
(187, 'Kökény Nóra', 'QHWO3I', 'kokenyist@gportal.hu', 0, '62fab6200cf9f014e3c9a892d1e7c2f912b66dff'),
(188, 'Németh Dóra', 'QKLIBH', 'ndora98@citromail.hu', 0, '62b5c24799e256c239ebacc9fe6db2fc40185b7a'),
(189, 'Kiss Márton', 'QWDA4Z', 'kmarton80@gmail.com', 0, '6aa734310a9fb494d6f0c465f7405f8f24772b31'),
(190, 'Szegfű Anna', 'QYE7WE', 'szegfupanni@gmail.com', 0, '76614138bcddd1fdc24b5ff2fb940ab5ceefbfb7'),
(191, 'Pirtyák Dorottya Magdolna', 'QZXTFF', 'dpirtyak@gmail.com', 0, '2fa208493e5ed165edd5add3f5156436fa73d772'),
(192, 'Haszonics Tamás', 'Q0RMFX', 'haszonics.tamas0813@gmail.com', 0, '5ddb1e43002526f436cefdbbfb51045a5f4463d3'),
(193, 'Standi Barnabás', 'Q1N3NO', 'standibarni@gmail.com', 0, 'df5adc7f2e770eef76d16da6ddd3d768d61babb3'),
(194, 'Otgonbayar Shiravjamts', 'Q5LIBJ', 'o_shiravjamts@yahoo.com', 0, 'b0d6871d050ed02e31d731b1edab676dd6080d17'),
(195, 'Tóth Lajos', 'RDMWSF', 'tothlajos2101@gmail.com', 0, '21e392957d036a1c1f76a1a89e7b0b3fb0b526d9'),
(196, 'Vass Veronika', 'RLOIST', 'vass.vera23@gmail.com', 0, 'f74252bd4b52ff3857cca8a0ba91db201de287a0'),
(197, 'Szijj Réka', 'RTUS6T', 'reka.szijj@gmail.com', 0, '9c886b9fa824eefa77f39c9f922df0f7b2ef7cba'),
(198, 'Németh Patrik', 'RVQQOR', 'patrik8813@gmail.com', 0, 'bd12fd8a7c8069777b4e3c47d3be45599e4e0278'),
(199, 'Horváth Szandra Viktória', 'SC3SIW', 'horvath.szandra@zelkanet.hu', 0, '0a21fe095832b2e232054e2d75b13435237748df'),
(200, 'Kelemen Áron', 'SUN9OC', 'aronka11@gmail.com', 0, 'ff5a5cd1184fac0afd546141bbb3ed2523ae4846'),
(201, 'Juhász Gergő', 'SYYC6T', 'gergojuhasz96@gmail.com', 0, 'ce14eb4b21e200287d7830c67d7a72f0ee013926'),
(202, 'Hegedüs Benjámin Jutas', 'SY948Q', 'benjaminjutas@gmail.com', 0, 'e2ecd9bbd6e5ddc68621404577f1a552bfac917a'),
(203, 'Szabó Zsuzsanna', 'SZBGYA', 'szzsuzsika.96@gmail.com', 0, '83ddb621ece503d245185dce12f751c98a9d2b56'),
(204, 'Kutasi Dóra Mária', 'S5UQ75', 'fbdoria@freemail.hu', 0, '98eb29cf709ccc10115dd4e85310d061b2c9ff56'),
(205, 'Balogh Eszter', 'S9CLGV', 'eszti078@gmail.com', 0, '38b10387ddd55212139fd1275f332de3afb6dadf'),
(206, 'Antal Áron', 'TJB0I5', 'antalaron17@gmail.com', 0, 'cbba42d6fb7b2aeba0ddb56aa2eb8061eea9e758'),
(207, 'Bakos Nikolett', 'TK9FHV', 'bakos.nikolett98@gmail.com', 0, 'db4722387e28571a3f2abeff28d421721e93aae2'),
(208, 'Pásztor Tamás', 'TOWGP3', 'pasztortamas2000@gmail.com', 0, '8c0b27d61bae37cc473e3d71f487687bac230fa3'),
(209, 'Hodics Petra', 'TQPXCA', 'hodicspetra@gmail.com', 0, 'a27278a2a8dbf0e0c485d4cf558624bf86edc47c'),
(210, 'Kis Tibor Dániel', 'TX4DT8', 'tibor.daniel.kis@icloud.com', 0, 'c573fd12cbf4fe3bb0c8a84bd12aa00a0929d444'),
(211, 'Pásztor Mirandolína Júlia', 'UB0561', 'mira.linu@gmail.com', 0, '6de38ea8783186b08fd22c301c3e7cf6fd1c7a98'),
(212, 'Szalai Enikő', 'UL8048', 'szalayency@freemail.hu', 0, 'a4c37769411d0b1b2c9b7c8e4fea83f47d801055'),
(213, 'Csipak Edvin', 'UPIW38', 'csipakedvin@hotmail.com', 0, '2bb7c6668a2db0cd78a983dc54a9914f7cbc56d2'),
(214, 'Bukta Bence', 'UQ1GZD', 'bbukta@icloud.com', 0, '4ce4d506a698e7ec49fa45cc54df0067e5a3f86c'),
(215, 'Orcsik Dávid', 'US4GMR', 'david.orcsik.prog@gmail.com', 0, '05f95951225d591cc7fd5863b794ed8d70c8b399'),
(216, 'Yukihira Kenichi', 'UU43P9', 'tyanyuki3.39@gmail.com', 0, '439d6ed812a645f3a93dec196abb3ac0cba4ce78'),
(217, 'Berkics Karola Hanna', 'UZV8JV', 'berkicskarola@gmail.com', 0, '4f3879570a9b1e78071cb6babac73aac011e6a93'),
(218, 'Tóth Norbert', 'U9MOAD', 'u9moad@inf.elte.hu', 0, 'fa5670cdd676a973a0cfe40172666178eb2f35dd'),
(219, 'Szabó Dorina Szabina', 'U990JH', 'dorinaszabina@gmail.com', 0, '73faf27db467a0d69aa33828e7f472bae1c1b5cf'),
(220, 'Tóth Dominik', 'VDEDX8', 'tuti.1997@freemail.hu', 0, '7ccb6cddee20b5b76c6a652734c48fdd5f3130b3'),
(221, 'Bogdán Fanni', 'VYGKZ6', 'bogdanfanni96@gmail.com', 0, 'dc968f6f3753dd4ac15b1f186579c7abbe3bed41'),
(222, 'Konderková Vanessa', 'V0VON3', 'konderkavanessa@gmail.com', 0, '0864054233e9e9cde6b73c6b039b4cbcd10b750f'),
(223, 'Bimbó Nóra Erika', 'WB027O', 'bnora8296@gmail.com', 0, 'd69a2764499500ee4bc594bbf1964c714e8612f5'),
(224, 'Deák Vince', 'WIWPTL', 'deakvince@gmail.com', 0, '9ec9291ddb361e5f0912070c3774cf937c352a4e'),
(225, 'Bak Xavér Ferenc', 'WM4RQ1', 'bak.xaver@gmail.com', 0, '4d7bd22902b4f0de94038ca9fa9f972889e0b8db'),
(226, 'Kovács Áron', 'WQ8YCS', 'kovacsari@gmail.com', 0, '9267b87099eaeeb6be882f2e09633dc3a1da51a5'),
(227, 'Nagy Tamás Norbert', 'WWZ3K0', 'ntamas1994@gmail.com', 0, '726083d06e63930c9a091c23e483bf6595292d35'),
(228, 'Szabó Karina Anita', 'WZD8NH', 'wearethercyclones@gmail.com', 0, '75b1f3a6213795a3e1349e4dab6919b7553004f6'),
(229, 'Donkó Rebeka Mária', 'W5DABJ', 'reba9736@gmail.com', 0, 'b5b3036f56998e143f8aa8351c127b300b76a318'),
(230, 'Szűcs Bence Soma', 'W6PB2D', 'szucssoma@gmail.com', 0, 'a2af7c7b9d68e8795aa7d500f5fbaa21b94767ba'),
(231, 'Kriston Dániel', 'XDLP97', 'k.danihun@windowslive.com', 0, 'bfd9fc2504000c085ec47e45645e63fd972e366a'),
(232, 'Halasi Vivien', 'XJU7XH', 'halasivivi@gmail.com', 0, 'd06fc6f93e53bc1b800a1c35748a6af38ef8ab67'),
(233, 'Nesic Regina Nóra', 'XK66J8', 'nesiczregi@gmail.com', 0, 'a338cf8f599bda45733f55bb0fb8ad93e15bee84'),
(234, 'Caki Gönül', 'XK9OI2', 'g-n-l-caki@hotmail.com', 0, 'd2f45e512da39788edf312f871ac381a0642cdd6'),
(235, 'Beksi Anna Dorottya', 'XMOV5M', 'beksianna@gmail.com', 0, 'd32d69b4987986213981d8e45fa5a7acd9861852'),
(236, 'Horváth Ádám', 'XY4DFW', 'adam19960903@gmail.com', 0, '3431d388aa0929c81cff98d86233843ab55da76c'),
(237, 'Polereczki Fanni', 'X3N4XV', 'Tamiwen23@gmail.com', 0, 'fafb235b3a4b91c8354be0d6f2c2eb72b6abe9e2'),
(238, 'Brunner Balázs', 'X8TANY', 'bundyy20@gmail.com', 0, '39e330352c5d6cd742a212d9763057b1c1523c41'),
(239, 'Nagy Martina Viktória', 'YAF0GU', 'nagymartina0113@gmail.com', 0, '45414d4cb4a9a1fc58510dbf485845cbb1a730d7'),
(240, 'Varga Gyula', 'YAV45W', 'vagynap22894@gmail.com', 0, 'a0f4fe1610d7c51ebb24894f74ce4cf4f26b8f11'),
(241, 'Csonka Dávid', 'YCTVRH', 'csonda5@gmail.com', 0, '306fc1fdc25b9e0cb1caab78e0948eabd18b9df3'),
(242, 'Bogdány Máté', 'YIQSIX', 'bogdanymate@gmail.com', 0, 'f0f2ac45b5ba7debb2fbe8cccb4d182d78add4e9'),
(243, 'Sókyová Blanka', 'YR47X2', 'Blankasokyova@gmail.com', 0, 'a5fb27977a38bfdbadd711db0402e7faf56a7460'),
(244, 'Pomázi Bence', 'YVC6EB', 'pomazi.bence@gmail.com', 0, '00e1a1255aa2add18810bebbf8e0fc58cd742469'),
(245, 'Vass Luca', 'YYOV8E', 'luca.vass@gmail.com', 0, '79f4a31a20a53fe4af38665be690953069fd17c2'),
(246, 'Brindza Tímea', 'Y100WM', 'brindzatimi@gmail.com', 0, 'f07203772e577203ad9ed06ea2b65482c91af123'),
(247, 'Bukor Ákos', 'Y3WVHP', 'bidu006@gmail.com', 0, 'b68cbbf71cde7729a0854bf1c7d64184c34d94ca'),
(248, 'Sánta Krisztina', 'Y7X296', 'santa.krisztina96@gmail.com', 0, 'ed41767db70e728c2888dcef201064b13fc67c1f'),
(249, 'Timár Petra', 'ZCQH2L', 'timarpetra99@gmail.com', 0, 'c0f6f0dbefb8fcf6d5c6560d4a9627aa813da47b'),
(250, 'Kertész Zoltán', 'ZD02DV', 'gkacska@gmail.com', 0, '80532ae33da131a652de5946e31fef495ede82e6'),
(251, 'Molnár Csenge', 'ZFFW5P', 'mocsenge@gmail.com', 0, 'd094231ba2626dae0a2f6339c53e61742d8bf240'),
(252, 'Rózsás Patrícia', 'ZGAOX8', 'patricia.rozsas@gmail.com', 0, '660f66bfc9f9a2d54a26262494f3811a90b2c570'),
(253, 'Madaras Márton', 'ZLYIZC', 'madaras.marton2@gmail.com', 0, 'dd8fd110230cbde17a59e9f44b21fa2f6753c271'),
(254, 'Szilágyi Bernadett Ágnes', 'ZXAK6B', 'szilagyibernadetta@gmail.com', 0, 'd817bef03c0c4198e258e6b50f58026e31e04373'),
(255, 'Hetyey Botond', 'ZXR5B5', 'hetyey.botond@gmail.com', 0, '9a36520f502d838e01e8bbde46b2b8d52205e50a'),
(256, 'Böröndi Emília Anna', 'Z1KDP0', 'emiborondi@gmail.com', 0, '4610da0fcbda32056b6774403d1298312f8e6905'),
(257, 'Muraközy Berta', 'Z3CDK0', 'murakozy.berta@gmail.com', 0, '10dca64ed200773f6f5c286855703651052ad465');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `room_number` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `level`, `room_number`) VALUES
(901, 9, 1),
(902, 9, 2),
(903, 9, 3),
(904, 9, 4),
(905, 9, 5),
(906, 9, 6),
(907, 9, 7),
(908, 9, 8),
(909, 9, 9),
(910, 9, 10),
(911, 9, 11),
(912, 9, 12),
(913, 9, 13),
(914, 9, 14),
(915, 9, 15),
(801, 8, 1),
(802, 8, 2),
(803, 8, 3),
(804, 8, 4),
(805, 8, 5),
(806, 8, 6),
(807, 8, 7),
(808, 8, 8),
(809, 8, 9),
(810, 8, 10),
(811, 8, 11),
(812, 8, 12),
(813, 8, 13),
(814, 8, 14),
(815, 8, 15),
(701, 7, 1),
(702, 7, 2),
(703, 7, 3),
(704, 7, 4),
(705, 7, 5),
(706, 7, 6),
(707, 7, 7),
(708, 7, 8),
(709, 7, 9),
(710, 7, 10),
(711, 7, 11),
(712, 7, 12),
(713, 7, 13),
(714, 7, 14),
(715, 7, 15),
(601, 6, 1),
(602, 6, 2),
(603, 6, 3),
(604, 6, 4),
(605, 6, 5),
(606, 6, 6),
(607, 6, 7),
(608, 6, 8),
(609, 6, 9),
(610, 6, 10),
(611, 6, 11),
(612, 6, 12),
(613, 6, 13),
(614, 6, 14),
(615, 6, 15),
(501, 5, 1),
(502, 5, 2),
(503, 5, 3),
(504, 5, 4),
(505, 5, 5),
(506, 5, 6),
(507, 5, 7),
(508, 5, 8),
(509, 5, 9),
(510, 5, 10),
(511, 5, 11),
(512, 5, 12),
(513, 5, 13),
(514, 5, 14),
(515, 5, 15),
(401, 4, 1),
(402, 4, 2),
(403, 4, 3),
(404, 4, 4),
(405, 4, 5),
(406, 4, 6),
(407, 4, 7),
(408, 4, 8),
(409, 4, 9),
(410, 4, 10),
(411, 4, 11),
(412, 4, 12),
(413, 4, 13),
(414, 4, 14),
(415, 4, 15),
(301, 3, 1),
(302, 3, 2),
(303, 3, 3),
(304, 3, 4),
(305, 3, 5),
(306, 3, 6),
(307, 3, 7),
(308, 3, 8),
(309, 3, 9),
(310, 3, 10),
(311, 3, 11),
(312, 3, 12),
(313, 3, 13),
(314, 3, 14),
(315, 3, 15),
(201, 2, 1),
(202, 2, 2),
(203, 2, 3),
(204, 2, 4),
(205, 2, 5),
(206, 2, 6),
(207, 2, 7),
(208, 2, 8),
(209, 2, 9),
(210, 2, 10),
(211, 2, 11),
(212, 2, 12),
(213, 2, 13),
(214, 2, 14),
(215, 2, 15),
(101, 1, 1),
(102, 1, 2),
(103, 1, 3),
(104, 1, 4),
(105, 1, 5),
(106, 1, 6),
(107, 1, 7),
(108, 1, 8),
(109, 1, 9),
(110, 1, 10),
(111, 1, 11),
(112, 1, 12),
(113, 1, 13),
(114, 1, 14),
(115, 1, 15),
(1, 0, 1),
(2, 0, 2),
(4, 0, 4),
(5, 0, 5),
(6, 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `neptun_id` varchar(10) NOT NULL,
  `salt` varchar(20) NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `neptun_id`, `salt`, `token`) VALUES
(206, 'US4GMR', '27760', 'ffcc91dcda15ca720a4e4d7fea12c6db8d258420');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `new_layout`
--
ALTER TABLE `new_layout`
  ADD UNIQUE KEY `unique_resident_id` (`resident_id`),
  ADD KEY `index_room_id` (`room_id`);

--
-- Indexes for table `old_layout`
--
ALTER TABLE `old_layout`
  ADD UNIQUE KEY `unique_resident_id` (`resident_id`),
  ADD KEY `index_room_id` (`room_id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_email` (`email`),
  ADD KEY `index_neptun` (`neptun_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_level` (`level`),
  ADD KEY `index_room_number` (`room_number`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `neptun_id` (`neptun_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
