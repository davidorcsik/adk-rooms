GRANT USAGE ON *.* TO 'room_app'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON `room_app_db`.`new_layout` TO 'room_app'@'localhost';

GRANT SELECT ON `room_app_db`.`people` TO 'room_app'@'localhost';

GRANT EXECUTE ON PROCEDURE `room_app_db`.`get_old_room_occupancy` TO 'room_app'@'localhost';

GRANT EXECUTE ON PROCEDURE `room_app_db`.`is_session_token_valid` TO 'room_app'@'localhost';

GRANT EXECUTE ON PROCEDURE `room_app_db`.`get_room_occupancy` TO 'room_app'@'localhost';

GRANT EXECUTE ON PROCEDURE `room_app_db`.`generate_session_token` TO 'room_app'@'localhost';

GRANT EXECUTE ON PROCEDURE `room_app_db`.`get_user_info` TO 'room_app'@'localhost';
